﻿using UnityEngine;
using System.Collections;

public class MoveAwayFromPlayer : MonoBehaviour {
	GameObject Player;

	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update () {
		Vector3 Target = transform.position + (transform.position - Player.transform.position).normalized;





		transform.position = Vector3.MoveTowards (transform.position, Target, 0.5f * Time.deltaTime);
	}
}
