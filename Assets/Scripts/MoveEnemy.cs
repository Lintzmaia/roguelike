﻿using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour {
	public float MinDistance;

	public LayerMask myLayer;
	public float Vel = 5;
	PlayerTracker Player;
	void Awake () {
		NeutralVel = Vel;
		Player = (PlayerTracker)FindObjectOfType (typeof(PlayerTracker));
		MinDistance = GetComponent<AIManeger> ().DescicionDistance;
	}
	float NeutralVel;
	public void SlowDown(int Factor){
		if (Factor > 0) {
			Vel = NeutralVel / Factor;
		} else {
			Vel = 0;
		}
	}

	public void BackToNeutralVel(){
		Vel = NeutralVel;
	}

	public bool Towards;
	public bool Away;

	void Update () {
		Vector3 Target = transform.position;

		if (Towards) {
			Target = Player.P_Pos;
		} else if (Away) {
			Target = transform.position + (transform.position - Player.P_Pos).normalized;
		}

		float step = Vel * Time.deltaTime;
		RaycastHit Hit;
		if (Physics.Raycast (transform.position, new Vector3 (0, Target.y - transform.position.y, 0),
	                     out Hit, 0.5f, myLayer)) {

			Target = new Vector3 (Target.x, transform.position.y, 0);	

		}
		if (Physics.Raycast (transform.position, new Vector3 (Target.x - transform.position.x, 0, 0), 
	                     out Hit, 0.5F, myLayer)) {

			Target = new Vector3 (transform.position.x, Target.y, 0);

		}
		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (0, Target.y - transform.position.y, 0),
		                     out Hit, 0.5f, myLayer)) {
			
			Target = new Vector3 (Target.x, transform.position.y, 0);	
			
		}
		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (Target.x - transform.position.x, 0, 0), 
		                     out Hit, 0.5f, myLayer)) {
			
			Target = new Vector3 (transform.position.x, Target.y, 0);
			
		}

		transform.position = Vector3.MoveTowards (transform.position, Target, step);
	} 

}
