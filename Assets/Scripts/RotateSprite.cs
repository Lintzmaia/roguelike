﻿using UnityEngine;
using System.Collections;

public class RotateSprite : MonoBehaviour {

	public bool CanMove;
	public Sprite[] DirectionSprite;

	bool LookingDown;
	bool LookingUp;
	bool LookingLeft;
	bool LookingRight;

	Vector3 CurM_P;
	Vector3 PrevM_P;

	Animator Anim;
	void Start(){	
		PrevM_P = Input.mousePosition;
		Anim = GetComponentInChildren<Animator> ();
	}
	public bool MouseTrack;
	public bool JoystickTrack;
	void Update () {
		Vector3 DeltaMouse;
		CurM_P = Input.mousePosition;
		DeltaMouse = CurM_P - PrevM_P;
		PrevM_P = CurM_P;
		if (DeltaMouse.magnitude > 0.1) {
			MouseTrack = true;
			JoystickTrack = false;
		}
		if (new Vector3 (Input.GetAxis ("Joystick Horizontal"), Input.GetAxis ("Joystick Vertical"), 0).magnitude > 0.1) {
			MouseTrack = false;
			JoystickTrack = true;
		}


		if (MouseTrack) {

			Vector3 m_TankPos = Camera.main.WorldToScreenPoint (transform.position);

			Vector3 m_MouseDirection = Input.mousePosition;

			m_MouseDirection.x = m_MouseDirection.x - m_TankPos.x;
			m_MouseDirection.y = m_MouseDirection.y - m_TankPos.y;
			m_MouseDirection.Normalize ();

			CanMove = GetComponentInChildren<DealDamege> ().CanDeal;



			if (!CanMove) {
				GetComponentInChildren<DealDamege> ().GetAim (m_MouseDirection);
				if (!(Mathf.Abs (m_MouseDirection.x) == Mathf.Abs (m_MouseDirection.y))) {		
					if (Mathf.Abs (m_MouseDirection.x) > Mathf.Abs (m_MouseDirection.y)) {
						if (m_MouseDirection.x < 0) {
							//Looking Left
							if (!LookingLeft) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [0];							
								LookingDown = false;
								LookingUp = false;
								LookingRight = false;
								LookingLeft = true;

								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Left", false);
							}

						} else {
							//Looking Right
							if (!LookingRight) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [2];

								LookingDown = false;
								LookingUp = false;
								LookingRight = true;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Right", false);
							}
						}
					} else {
						if (m_MouseDirection.y < 0) {
							//Looking Down
							if (!LookingDown) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [1];

								LookingDown = true;
								LookingUp = false;
								LookingRight = false;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);

							} else {
								Anim.SetBool ("Look Down", false);
							}
						} else {
							//Looking Up
							if (!LookingUp) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [3];

								LookingDown = false;
								LookingUp = true;
								LookingRight = false;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Up", false);
							}
						}
					}
					GetComponentInChildren<SwingSword> ().AjustLayerSword (LookingUp, LookingDown, LookingLeft, LookingRight);
				}
			}
		} else if (JoystickTrack) {
			Vector3 Direction = new Vector3(Input.GetAxis("Joystick Horizontal"), Input.GetAxis("Joystick Vertical"), 0);


			CanMove = GetComponentInChildren<DealDamege> ().CanDeal;
			

			
			if (!CanMove && Direction.magnitude > 0.50f) {
				GetComponentInChildren<DealDamege> ().GetAim (Direction);
				if (!(Mathf.Abs (Direction.x) == Mathf.Abs (Direction.y))) {		
					if (Mathf.Abs (Direction.x) > Mathf.Abs (Direction.y)) {
						if (Direction.x < 0) {
							//Looking Left
							if (!LookingLeft) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [0];							
								LookingDown = false;
								LookingUp = false;
								LookingRight = false;
								LookingLeft = true;
								
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Left", false);
							}
							
						} else {
							//Looking Right
							if (!LookingRight) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [2];
								
								LookingDown = false;
								LookingUp = false;
								LookingRight = true;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Right", false);
							}
						}
					} else {
						if (Direction.y < 0) {
							//Looking Down
							if (!LookingDown) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [1];
								
								LookingDown = true;
								LookingUp = false;
								LookingRight = false;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
								
							} else {
								Anim.SetBool ("Look Down", false);
							}
						} else {
							//Looking Up
							if (!LookingUp) {
								GetComponent<SpriteRenderer> ().sprite = DirectionSprite [3];
								
								LookingDown = false;
								LookingUp = true;
								LookingRight = false;
								LookingLeft = false;
								Anim.SetBool ("Look Down", LookingDown);
								Anim.SetBool ("Look Left", LookingLeft);
								Anim.SetBool ("Look Right", LookingRight);
								Anim.SetBool ("Look Up", LookingUp);
							} else {
								Anim.SetBool ("Look Up", false);
							}
						}
					}
					GetComponentInChildren<SwingSword> ().AjustLayerSword (LookingUp, LookingDown, LookingLeft, LookingRight);
				}
			}

		}
	}
	public void AssignBool(bool Bool){
		CanMove = Bool;
	}



}
