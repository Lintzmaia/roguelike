﻿// Get the Classes (bibliotecs) that will be referred in the code
using UnityEngine; // basic class
using System; // basic class
using System.Collections;
using System.Collections.Generic; // pull the 'List' class, that is, roughlly, a array with dynamic size
using Randon = UnityEngine.Random; // best random generator class in unity

public class CallNewRoom : MonoBehaviour {

	public PlayerTracker player;
	void Awake(){
		ResetDoorBools ();
	}

	Camera MainCamera;


	void Start(){
	player = (PlayerTracker)FindObjectOfType (typeof(PlayerTracker));
	GameObject.FindGameObjectWithTag("MainCamera");
	}
	void Update(){
	if (player.Distance (transform.position) <= 0.7) {
		Test();
	}
		Test();
}
	public void Test () {		


		if (Door.Length < 100) {
			switch (gameObject.name) {
		case "Left Door":			
			if (!Physics.Raycast(transform.position + new Vector3(-1, 0, -3), new Vector3(0, 0, 1))){
			RightDoor = true;
			NewRoomPos = new Vector3 (-Mathf.Abs (PreviousRoom.x - coluns - 2), 0, 0);
				Run ();
			}
			else {
				this.enabled = false;
			}
				break;
		case "Top Door":
			if (!Physics.Raycast(transform.position + new Vector3(0, 1, -3), new Vector3(0, 0, 1))){
			BottunDoor = true;
				NewRoomPos = new Vector3 (0, Mathf.Abs (PreviousRoom.y) + lines + 2, 0);
				Run ();
			}else{
				this.enabled = false;
			}
				break;
		case "Right Door":
			if (!Physics.Raycast(transform.position + new Vector3(1, 0, -3), new Vector3(0, 0, 1))){
			LeftDoor = true;
				NewRoomPos = new Vector3 (Mathf.Abs (PreviousRoom.x + coluns + 2),0, 0);
				Run ();
			}else{
				this.enabled = false;
			}
				break;
		case "Bottun Door":
			if (!Physics.Raycast(transform.position + new Vector3(0, -1, -3), new Vector3(0, 0, 1))){
			TopDoor = true;
				NewRoomPos = new Vector3 (0, -Mathf.Abs (PreviousRoom.y) - lines - 2, 0);
				Run ();
			}else{
				this.enabled = false;
			}
				break;		
			}
		}
	}
	[Serializable]		//the only reason this is here is because it is prettier in the inspector 
	public class Count	//whem you call the class in the code, take the Obstacle variable in line 25 as exemple
	{
		public int Minimun;
		public int Maximun;

		public Count( int Min, int Max) // With this constructor you can hold 2 values. if this wasan't made, the 
		{									// would became a monster to understand later, because would have to call 
			Minimun = Min;					// two int for a minimun and maximun every time
			Maximun = Max;
		}

	}
	// Some nunbers to set the Scene for this screen
	public int lines = 5;									//Number of lines
	public int coluns = 7;									//Nunber of coluns
	public Count Obstacles = new Count (30, 40);			//Holds values that will be used again. A relation beteween area and
	//amount of obstacles can be made during the polishing state

	//Some arrays that form a pool of what tile will be used
	public GameObject[] FloorTiles;
	public GameObject[] Border;
	public GameObject[] Corner;
	public GameObject[] Door;
	public GameObject[] DoorNoCollider;
	public GameObject[] DestructbleWall;
	public GameObject[] ObstacleTiles;						//Solid object, like walls or trees


	//Transform is a class that holds Space coordanaes, such as position ( x, y, z ), rotation and scale of the object
	Transform WorldSpaceBoard;							//Private because no other script will access it
	private List <Vector3> GridPosition = new List<Vector3>();	// a list is just a array that has no defined size

	//Method to make sure there are no data from previous room or scene. Just prepares the variables
	void PreGrid()								
	{
		GridPosition.Clear ();

		for ( int x = 1; x < coluns - 1; x++)	//It starts at one just to make sure the borders will be free, since all points in this list
		{										//have a chance to spawn a obstacle in it.
			for (int y  = 1; y < lines - 1; y++)
			{
				GridPosition.Add(new Vector3(x, y, 0)); //Add the point that any stuff can spawn to the list

			}
		}
	}

	public bool NotFirstBoard;



	public Vector3 PreviousRoom;


	public bool RightDoor;
	public bool BottunDoor;
	public bool LeftDoor;
	public bool TopDoor;

	public LayerMask myLayer;

	public GameObject Room;


public LayerMask DoorLayerMask;
	//Spawn a tile
	void GroundAndWalls()
	{		//The reason a game object was created is just to Organize the interface of the engine

		
		Room = new GameObject ("Room"); 
		Room.tag = "Room";
		WorldSpaceBoard = Room.transform;					//Gets the space values of this, 
															//so all calculation are based in a
															//plane that has origin in this object


		for (int x = -1; x < coluns + 1; x++) 
		{
			for(int y = -1; y < lines + 1; y++)
			{

				GameObject floorInPoint = 
					Instantiate( FloorTiles[ Randon.Range (0, FloorTiles.Length)],	//Chooses a tile floor to put in the current point 
						new Vector3(x, y, 0),								// Position it will spawn
						Quaternion.identity) as GameObject; 					//quaternion.identity = no rotation
				if (x == -1 || x == coluns || y == -1 || y == lines)		//if the current point it's in the border of the room
				{
					if(Border.Length > 1 && Corner.Length == 0){
						GameObject wallInPoint = Instantiate(Border[Randon.Range(0, Border.Length)],
							new Vector3(x,y,0),
							Quaternion.identity) as GameObject;
						wallInPoint.transform.SetParent(WorldSpaceBoard); 			//Link the wall tile position to the WorldSpaceBoard
					}
					if (Corner.Length == 1 && Border.Length == 1){
						if(x == -1 && y == -1){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0 , 0, 90)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns && y == -1){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 180)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns && y == lines){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 270)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == -1 && y == lines){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);

						}
						else if(x == -1){
							GameObject wallInPoint = Instantiate (Border [0], new Vector3 (x, y, 0), Quaternion.Euler (0, 0, 0)) as GameObject;
							
							wallInPoint.layer = 9;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 180)) as GameObject;
							wallInPoint.layer = 9;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(y == -1){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
							wallInPoint.layer = 10;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(y == lines){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 270)) as GameObject;
							wallInPoint.layer = 10;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
					}
				}
				floorInPoint.transform.SetParent(WorldSpaceBoard);				//Does the same as above
			}
		}


		ExitLevelTiles (WorldSpaceBoard);

	}
	public Vector3 NewRoomPos;
	public int MaxDoor;
	private int NunOfExits;
	void ExitLevelTiles(Transform WorldSpaceBoard){


			

			
			NunOfExits = Randon.Range (1, 3);
		if (GameObject.FindGameObjectsWithTag ("Door").Length <= MaxDoor / 5) {
			NunOfExits = Randon.Range (1, 2);
		} else if (GameObject.FindGameObjectsWithTag ("Door").Length <= MaxDoor / 2.5f) {
			NunOfExits = Randon.Range (1, 1);
		} else if (GameObject.FindGameObjectsWithTag ("Door").Length <= MaxDoor / 1.6f) {
			NunOfExits = 1;
		} else if (GameObject.FindGameObjectsWithTag ("Door").Length > MaxDoor / 1.6f) {
			NunOfExits = 0;
		}
	Debug.Log (NunOfExits);
			
	if (GameObject.FindGameObjectsWithTag ("Door").Length <= MaxDoor) {
			for (int pass = 0; pass <= NunOfExits; pass++) {		
				
				if (BottunDoor && TopDoor && RightDoor && LeftDoor) {
				ResetDoorBools();
					return;
				}

				
				else{
				
					switch (Randon.Range (1, 5)) {
						
					case 1:
						if (!LeftDoor) {
							LeftDoor = true;
							float RandomPosition = (int)Randon.Range (1, lines - 1);			//Generate random place in line
							Vector3 DoorPos = new Vector3 (-1, RandomPosition, 3);				//Assign the point to a Vector3
							
							GameObject OrigemRay = new GameObject ("OriginRay");				//Object that will share Position with ray
							
							
							OrigemRay.transform.position = DoorPos;								//Set that object to that Vector3 


							
							OrigemRay.transform.SetParent (WorldSpaceBoard);						
							RaycastHit Hit;
							
							Ray CheckHit = new Ray (OrigemRay.transform.position, new Vector3 (0, 0, -1));					
							if (Physics.Raycast (CheckHit, out Hit, 10f, myLayer)) {
								if (Hit.collider.gameObject.tag != "Door") {
									GameObject.Destroy (Hit.collider.gameObject);
									GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0),
									                                    Quaternion.Euler (0, 0, 0)) as GameObject;
									DoorInPos.gameObject.name = "Left Door";
									DoorInPos.transform.SetParent (WorldSpaceBoard);

								}
								GameObject.Destroy (OrigemRay);
							}
							
						} 
						break;		
					case 2:
						if (!BottunDoor) {
							BottunDoor = true;
							float RandomPosition = (int)Randon.Range (1, coluns - 1);
							Vector3 DoorPos = new Vector3 (RandomPosition, -1, 3);
							GameObject OrigemRay = new GameObject ("OriginRay");
							OrigemRay.transform.position = DoorPos;
							OrigemRay.transform.SetParent (WorldSpaceBoard);
							RaycastHit Hit;						
							
							Ray CheckHit = new Ray (OrigemRay.transform.position, new Vector3 (0, 0, -1));
							if (Physics.Raycast (CheckHit, out Hit, 10f, myLayer)) {
								if (Hit.collider.gameObject.tag != "Door") {
									GameObject.Destroy (Hit.collider.gameObject);
									GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), 
									                                    Quaternion.Euler (0, 0, 90)) as GameObject;
									DoorInPos.gameObject.name = "Bottun Door";
									DoorInPos.transform.SetParent (WorldSpaceBoard);

								}
								GameObject.Destroy (OrigemRay);
							}
							
						} 
						break;
						
					case 3:
						if (!RightDoor) {
							RightDoor = true;
							float RandomPosition = (int)Randon.Range (1, lines - 1);
							Vector3 DoorPos = new Vector3 (coluns, RandomPosition, 3);
							GameObject OrigemRay = new GameObject ("OriginRay");
							OrigemRay.transform.position = DoorPos;
							OrigemRay.transform.SetParent (WorldSpaceBoard);
							RaycastHit Hit;

							Ray CheckHit = new Ray (OrigemRay.transform.position, new Vector3 (0, 0, -1));						
							if (Physics.Raycast (CheckHit, out Hit, 10f, myLayer)) {
								if (Hit.collider.gameObject.tag != "Door") {
									GameObject.Destroy (Hit.collider.gameObject);
									GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), 
									                                    Quaternion.Euler (0, 0, 180)) as GameObject;
									DoorInPos.gameObject.name = "Right Door";
									DoorInPos.transform.SetParent (WorldSpaceBoard);
								}
								GameObject.Destroy (OrigemRay);
								
							}
							
						}
						break;
						
					case 4:
						if (!TopDoor) {
							TopDoor = true;
							float RandomPosition = (int)Randon.Range (1, coluns - 1);
							Vector3 DoorPos = new Vector3 (RandomPosition, lines, 3);
							GameObject OrigemRay = new GameObject ("OriginRay");
							OrigemRay.transform.position = DoorPos;
							OrigemRay.transform.SetParent (WorldSpaceBoard);
							RaycastHit Hit;						
							
							Ray CheckHit = new Ray (OrigemRay.transform.position, new Vector3 (0, 0, -1));
							if (Physics.Raycast (CheckHit, out Hit, 10f, myLayer)) {
								if (Hit.collider.gameObject.tag != "Door") {
									GameObject.Destroy (Hit.collider.gameObject);
									GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0),
									                                    Quaternion.Euler (0, 0, 270)) as GameObject;
									DoorInPos.gameObject.name = "Top Door";
									DoorInPos.transform.SetParent (WorldSpaceBoard);
								}
								GameObject.Destroy (OrigemRay);							
							}
							
						}
						break;			
						
					}

				}
			}
		}
	}

	void ResetDoorBools(){
		RightDoor = false;
		BottunDoor = false;
		LeftDoor = false;
		TopDoor = false;
	}

	// Gets a random point, where something will ramdomlly be put
	Vector3 ObjectPlacementPoint()		//Vector3 because will return a (x, y, z) point in space
	{
		int randomIndexInList = Randon.Range (0, GridPosition.Count);	//ramdomlly gets a vector3 from the list of all availible position
		Vector3 Point = GridPosition [randomIndexInList];			//gets the variable in that index and assing to 'Point'

		GridPosition.RemoveAt (randomIndexInList);			//removes the index, so it don't spawn to trees in the same place

		return Point;	//throw the point back to the function that called it
	}


	//ramdomlly put stuff down in map. Such as loose drops, chests, enemys, walls, rocks
	void LayoutOfLooseObjects(GameObject[] ObjArray,				//Array of the loose object
		int minimun, int maximun)
	{	
		Transform SolidHolder = new GameObject("Holder").transform;
		SolidHolder.transform.position = new Vector3 (0, 0, 0);

		int amountOfStuff = 
			Randon.Range (minimun, maximun + 1); 					//A random roll to see the amount of the object that are the array
		for (int pass = 0; pass < amountOfStuff; pass++) 
		{
			Vector3 randomPosition = ObjectPlacementPoint();		//Calls the above method, ObjectPacementPoint, a random amount of times

			GameObject RandomObjectInArray =					//Gets a random object from the poll that is the ObjArray
				ObjArray[Randon.Range(0, ObjArray.Length)];		//Works even if theres only one object in array

			//Spawn the object
			GameObject Solid = Instantiate(RandomObjectInArray,
				randomPosition,
				Quaternion.identity) as GameObject;
			Solid.transform.SetParent(Room.transform);

		}
	}


	//The method that will call all the others above in the right order
	public void Run()								//Public method because other script will call it
	{


		GroundAndWalls();							//Put down Ground and Walls

		PreGrid ();									//create the list and fill it up with points (x, y, z)

		if (ObstacleTiles.Length > 0) {
			LayoutOfLooseObjects (ObstacleTiles,	//The array of solids
				Obstacles.Minimun,	
				Obstacles.Maximun);
		}



		Room.transform.position = GetComponentInParent<Transform>().transform.parent.position + NewRoomPos;

		Ray SwitchTile;
		RaycastHit Hit;
		GameObject RayTile;
		switch (gameObject.name) {
		case "Top Door":
			RayTile = new GameObject ("Ray");
			RayTile.transform.position = transform.position;
			RayTile.transform.SetParent (gameObject.GetComponentInParent<Transform> ().parent);
			SwitchTile = new Ray (RayTile.transform.position + new Vector3 (0, 1, 3), new Vector3 (0, 0, -1)); 
			if (Physics.Raycast (SwitchTile, out Hit, 10f, myLayer)) {
				GameObject.Destroy (Hit.collider.gameObject);
				GameObject.Instantiate (DoorNoCollider [Randon.Range (0, 1)], RayTile.transform.position + new Vector3(0, 1, 0) , 
			                                          Quaternion.Euler (0, 0, 90));
				GameObject.Destroy (RayTile);			
		}			
			break;	
		case "Bottun Door":
			RayTile = new GameObject ("Ray");
			RayTile.transform.position = transform.position;
			RayTile.transform.SetParent (gameObject.GetComponentInParent<Transform> ().parent);			
			SwitchTile = new Ray (RayTile.transform.position + new Vector3 (0, -1, 3), new Vector3 (0, 0, -1)); 			
			if (Physics.Raycast (SwitchTile, out Hit, 10f, myLayer)) {
				GameObject.Destroy (Hit.collider.gameObject);
				GameObject.Instantiate (DoorNoCollider [Randon.Range (0, 1)], RayTile.transform.position + new Vector3(0, -1, 0), 
			                        Quaternion.Euler (0, 0, 270));
				GameObject.Destroy (RayTile);
			}
			break;
		case "Right Door":
			RayTile = new GameObject ("Ray");
			RayTile.transform.position = transform.position;
			RayTile.transform.SetParent (gameObject.GetComponentInParent<Transform> ().parent);			
			SwitchTile = new Ray (RayTile.transform.position + new Vector3 (1, 0, 3), new Vector3 (0, 0, -1)); 			
			if (Physics.Raycast (SwitchTile, out Hit, 10f, myLayer)) {
				GameObject.Destroy (Hit.collider.gameObject);
				GameObject.Instantiate (DoorNoCollider [Randon.Range (0, 1)], RayTile.transform.position + new Vector3(1, 0, 0), 
				                        Quaternion.Euler (0, 0, 0));
				GameObject.Destroy (RayTile);
		}
		break;
		case "Left Door":
			RayTile = new GameObject ("Ray");
			RayTile.transform.position = transform.position;
			RayTile.transform.SetParent (gameObject.GetComponentInParent<Transform> ().parent);			
			SwitchTile = new Ray (RayTile.transform.position + new Vector3 (-1, 0, 3), new Vector3 (0, 0, -1)); 			
			if (Physics.Raycast (SwitchTile, out Hit, 10f, myLayer)) {
				GameObject.Destroy (Hit.collider.gameObject);
				GameObject.Instantiate (DoorNoCollider [Randon.Range (0, 1)], RayTile.transform.position + new Vector3(-1, 0, 0), 
			                        Quaternion.Euler (0, 0, 180));
				GameObject.Destroy (RayTile);
		}
		break;
	}
	GameObject[] DoorList = GameObject.FindGameObjectsWithTag("Door");
	Transform[] Walls = Room.GetComponentsInChildren<Transform> ();


	foreach (Transform W in Walls) {

			
		if(W.gameObject.layer == 8){
			Ray CheckWall;
		switch(W.gameObject.name){
			case "Top Door":

					RayTile = new GameObject ("Ray");
					RayTile.transform.position = W.transform.position;
					RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);	
					CheckWall = new Ray(RayTile.transform.position + new Vector3 (0, 1, 3),  new Vector3(0, 0, -1));
					if(Physics.Raycast(CheckWall, out Hit)){
					Instantiate(Border[Randon.Range(0, Border.Length)], W.position, Quaternion.Euler(0, 0, 270));
					GameObject.Destroy(W.gameObject);
				}
				break;

			case "Bottun Door":
					RayTile = new GameObject ("Ray");
					RayTile.transform.position = W.transform.position;
					RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);
					CheckWall = new Ray(RayTile.transform.position + new Vector3 (0, -1, 3),  new Vector3(0, 0, -1));
				if(Physics.Raycast(CheckWall, out Hit)){
					Instantiate(Border[Randon.Range(0, Border.Length)], W.position, Quaternion.Euler(0, 0, 90));
					GameObject.Destroy(W.gameObject);
				}
				break;

			case "Right Door":
					RayTile = new GameObject ("Ray");
					RayTile.transform.position = W.transform.position;
					RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);
					CheckWall = new Ray(RayTile.transform.position + new Vector3 (1, 0, 3),  new Vector3(0, 0, -1));
				if(Physics.Raycast(CheckWall, out Hit)){

					Instantiate(Border[Randon.Range(0, Border.Length)], W.position, Quaternion.Euler(0, 0, 180));
					GameObject.Destroy(W.gameObject);
				}
				break;

			case "Left Door":
					RayTile = new GameObject ("Ray");
					RayTile.transform.position = W.transform.position;
					RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);
					CheckWall = new Ray(RayTile.transform.position + new Vector3 (-1, 0, 3),  new Vector3(0, 0, -1));
				if(Physics.Raycast(CheckWall, out Hit)){
					Instantiate(Border[Randon.Range(0, Border.Length)], W.position, Quaternion.Euler(0, 0, 0));
					GameObject.Destroy(W.gameObject);
				}
				break;
			}
		}

		if(W.gameObject.layer == 9){				//Vertical Check
				RayTile = new GameObject ("Ray");
				RayTile.transform.position = W.transform.position;
				RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);		
				Ray Ray0 = new Ray(RayTile.transform.position + new Vector3 (0, 0, 3),  new Vector3(0, 0, -1));
				Ray Ray1 = new Ray(RayTile.transform.position + new Vector3 (-1, 0, 3),  new Vector3(0, 0, -1));
				Ray Ray2 = new Ray(RayTile.transform.position + new Vector3 (1, 0, 3),  new Vector3(0, 0, -1));
				Ray[] Cast = new Ray[2];
				Cast[0] = Ray1;
				Cast[1] = Ray2;

				for(int i = 0; i < Cast.Length; i++){

					if (Physics.Raycast(Cast[i], out Hit, 10, DoorLayerMask) && Physics.Raycast(Ray0, 10f)){



							if (Hit.collider.name == "Left Door"){

								foreach(GameObject Door in DoorList){
								if (Door.name == "Right Door" && Door.GetComponentInParent<Transform>().transform.parent.position == 
																			Room.GetComponent<Transform>().transform.position){

									Hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
									
									GameObject.Destroy(Door);

								}



								}
						GameObject.Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], Hit.collider.gameObject.transform.position,
						                       Quaternion.Euler(0, 0, 0));
							Vector3 ConectingPoint = Hit.collider.transform.position + new Vector3(-1, 0, 0);
							Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], ConectingPoint, 
							            Quaternion.Euler(0, 0, 180));

							}else if(Hit.collider.name == "Right Door"){
								foreach(GameObject Door in DoorList){
									if( Door.name == "Left Door" && Door.GetComponentInParent<Transform>().transform.parent.position == 
								   											Room.GetComponent<Transform>().transform.position){

									
									GameObject.Destroy(Door);
																			
									}



								}
						GameObject.Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], Hit.collider.gameObject.transform.position,
						                       Quaternion.Euler(0, 0, 180));
							Vector3 ConectingPoint = Hit.collider.transform.position + new Vector3(1, 0, 0);
							Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], ConectingPoint,
							            Quaternion.Euler(0, 0, 0));

						}
					}
				}
				GameObject.Destroy(RayTile);
			}
			if( W.gameObject.layer == 10){		//Horizontal Check

				RayTile = new GameObject ("Ray");
				RayTile.transform.position = W.transform.position;
				RayTile.transform.SetParent (Room.GetComponentInParent<Transform> ().parent);		
				Ray Ray0 = new Ray(RayTile.transform.position + new Vector3 (0, 0, 3),  new Vector3(0, 0, -1));
				Ray Ray1 = new Ray(RayTile.transform.position + new Vector3 (0, -1, 3),  new Vector3(0, 0, -1));
				Ray Ray2 = new Ray(RayTile.transform.position + new Vector3 (0, 1, 3),  new Vector3(0, 0, -1));
				Ray[] Cast = new Ray[2];
				Cast[0] = Ray1;
				Cast[1] = Ray2;


				for(int i = 0; i < Cast.Length; i++){
					
					if (Physics.Raycast(Cast[i], out Hit, 10, DoorLayerMask) && Physics.Raycast(Ray0, 10f)){
						
						

						if (Hit.collider.name == "Top Door"){
							
							foreach(GameObject Door in DoorList){
								if (Door.name == "Bottun Door" && Door.GetComponentInParent<Transform>().transform.parent.position == 
								    Room.GetComponent<Transform>().transform.position){
									
									GameObject.Destroy(Door);
								}
								break;
							}
						GameObject.Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], Hit.collider.gameObject.transform.position,
						                       Quaternion.Euler(0, 0, 270));
							Vector3 ConectingPoint = Hit.collider.transform.position + new Vector3(0, 1, 0);
							Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], ConectingPoint, 
							            Quaternion.Euler(0, 0, 90));
						}else if(Hit.collider.name == "Bottun Door"){

							foreach(GameObject Door in DoorList){

								if( Door.name == "Top Door" && Door.GetComponentInParent<Transform>().transform.parent.position == 
								   Room.GetComponent<Transform>().transform.position){
									
									
									GameObject.Destroy(Door);										
								}

								break;
							}
						GameObject.Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], Hit.collider.gameObject.transform.position,
						                       Quaternion.Euler(0, 0, 90));
							Vector3 ConectingPoint = Hit.collider.transform.position + new Vector3(0, -1, 0);
							Instantiate(DoorNoCollider[Randon.Range(0, DoorNoCollider.Length)], ConectingPoint,
							            Quaternion.Euler(0, 0, 270));
						}						
						
					}
				}
				GameObject.Destroy(RayTile);
			}									                         
	}

	GameObject.Destroy (this);
	}
}
