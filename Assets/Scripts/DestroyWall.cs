﻿using UnityEngine;
using System.Collections;

public class DestroyWall : MonoBehaviour {

	public LayerMask myLayerMask;
	Transform Self;

	// Use this for initialization
	void Start () {
		Self = GetComponent<Transform> ();

	}
	RaycastHit Hit;
	// Update is called once per frame
	void Update () {
		
		if (Physics.Raycast (Self.transform.position + new Vector3 (0, 0, -3), new Vector3 (0, 0, 1), out Hit, myLayerMask)) {

			if (Hit.collider.gameObject.tag == "Border") {
				
				GameObject.Destroy (Hit.collider.gameObject);
			}else if (Hit.collider.gameObject.tag == "Door"){

				GameObject.Destroy (Hit.collider.gameObject);
			}

		}
	}
}
