﻿using UnityEngine;
using System.Collections;

public class WakeUpScript : MonoBehaviour {

	public bool SpawnRoom;
	public static WakeUpScript instance = null;
	void Awake(){
		if (instance == null) {
			instance = this;
		}
		else if (instance != this){
			Destroy(gameObject);
		}
		DontDestroyOnLoad (gameObject);
		//gameObject.GetComponent<SetTiles> ().GetItReady ();
		SpawnRoom = false;
		gameObject.GetComponent<SetTiles>().Run ();
	
	}

	void Start () {
		
		/*transform.position = new Vector3 (GetComponent<SetTiles> ().coluns / 2,
		                                 GetComponent<SetTiles> ().lines / 2,
		                                 transform.position.z);*/
	}	


	void Update(){
		if (Input.GetKeyDown (KeyCode.R) || SpawnRoom) {
			//GameObject.Destroy(GameObject.Find("Room"));
			GetComponent<SetTiles>().NotFirstBoard = true;
			//GameObject.Destroy (GameObject.Find("Holder"));
			gameObject.GetComponent<SetTiles>().Run ();
			SpawnRoom = false;
		}
	}
}
