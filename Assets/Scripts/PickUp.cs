﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {
	public float PickUpRadius = 0.5f;

	void Start () {
		player = (PlayerTracker)FindObjectOfType (typeof(PlayerTracker));
	}
	public PlayerTracker player;

	void Update () {

		if (player.Distance(transform.position) < PickUpRadius) {
			player.AddCoin(tag);
			this.enabled = false;
			Destroy(gameObject);
		}
	}
}
