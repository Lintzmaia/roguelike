﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeRoom : MonoBehaviour {

	Camera MainCan;
	public LayerMask myLayer;

	Vector3 Target1;
	Vector3 Target2;

	bool MovePlayer1;
	bool MovePlayer2;

	List <GameObject> Enemys = new List<GameObject> ();
	List <GameObject> OtherEnemys = new List<GameObject> ();

	public LayerMask myLayerWall;
	public LayerMask myLayerDoor;
	RaycastHit Hit;

	void Start () {

		MainCan = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
	}
	float HAxis;
	float VAxis;
	public void CheckChangeRoom(float HAxis0, float VAxis0){

			HAxis = HAxis0;
			VAxis = VAxis0;


		if (Physics.Raycast (transform.position + new Vector3 (0, 0, 5), new Vector3 (HAxis, 0, 0), out Hit, 0.3f, myLayer)) {

			GetComponent<MovePlayer> ().enabled = false;
			GetComponentInChildren<SwingSword>().enabled = false;
			Target1 = Hit.collider.transform.position - new Vector3 (0, 0, Hit.collider.transform.position.z);
			
			if (HAxis > 0) {
				Target2 = Target1 + new Vector3 (2f, 0, 0);
				
				TurnAIOff(Hit.collider.GetComponentInParent<Transform>(), "Right");
				MainCan.GetComponent<ChangeCameraPos>().ChangePos("Right");
			} else if (HAxis < 0) {
				Target2 = Target1 + new Vector3 (-2f, 0, 0);
				
				
				TurnAIOff(Hit.collider.GetComponentInParent<Transform>(),"Left");
				MainCan.GetComponent<ChangeCameraPos>().ChangePos("Left");
			}
			MovePlayer1 = true;
		}
		if (Physics.Raycast (transform.position + new Vector3 (0, 0, 5), new Vector3 (0, VAxis, 0), out Hit, 0.3f, myLayer)) {

			GetComponent<MovePlayer> ().enabled = false;
			GetComponentInChildren<SwingSword>().enabled = false;
			Target1 = Hit.collider.transform.position - new Vector3 (0, 0, Hit.collider.transform.position.z);
			
			if (VAxis > 0) {
				Target2 = Target1 + new Vector3 (0, 2f, 0);
				
				
				TurnAIOff(Hit.collider.GetComponentInParent<Transform>(), "Up");
				MainCan.GetComponent<ChangeCameraPos>().ChangePos("Up");
			} else if (VAxis < 0) {
				Target2 = Target1 + new Vector3 (0, -2f, 0);
				
				
				TurnAIOff(Hit.collider.GetComponentInParent<Transform>(), "Down");
				MainCan.GetComponent<ChangeCameraPos>().ChangePos("Down");
			}
			MovePlayer1 = true;
		}
	}


	void TurnAIOff(Transform Door, string Direction){
		Transform Room = null;
		Enemys.Clear ();
		OtherEnemys.Clear ();
		switch (Direction) {
		case "Right":
			if (Physics.Raycast (Door.position + new Vector3 (1, 1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}else if (Physics.Raycast (Door.position + new Vector3 (1, -1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}
			break;
		case "Left":
			if (Physics.Raycast (Door.position + new Vector3 (-1, 1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}else if (Physics.Raycast (Door.position + new Vector3 (-1, -1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}
			break;
		case "Up":
			if (Physics.Raycast (Door.position + new Vector3 (1, 1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			} else if (Physics.Raycast (Door.position + new Vector3 (-1, 1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}
			break;
		case "Down":
			if (Physics.Raycast (Door.position + new Vector3 (1, -1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			} else if (Physics.Raycast (Door.position + new Vector3 (-1, -1, 3), new Vector3 (0, 0, -1), out Hit, myLayerWall)) {
				Room = Hit.collider.transform.parent;
			}
			break;
		}

	
		GameObject[] List = GameObject.FindGameObjectsWithTag("Enemy");

		foreach (GameObject Obj in List) {
		

			Obj.GetComponent<AIManeger> ().SlowDown (5);

			if (Obj.transform.parent == Room) {
				Enemys.Add (Obj);

				//Obj.GetComponent<MoveTowardsPlayer>().enabled = true;
			} else {
				OtherEnemys.Add (Obj);
				//		Obj.GetComponent<MoveTowardsPlayer>().enabled = false;
			}
		}

	}




	void Update(){
		if (MovePlayer1) {
			transform.position = Vector3.MoveTowards (transform.position, Target1, 2 * Time.deltaTime);
			if (transform.position == Target1) {
				MovePlayer2 = true;
				MovePlayer1 = false;
			}
		} if (MovePlayer2) {

			transform.position = Vector3.MoveTowards (transform.position, Target2, 2 * Time.deltaTime);
			if (transform.position == Target2) {
				MovePlayer2 = false;
				StartCoroutine(Wait());
				GetComponent<MovePlayer>().enabled = true;
				GetComponent<MovePlayer>().ResetFactor();
				GetComponentInChildren<SwingSword>().enabled = true;
			}
		}
	}
	public float Timer;
	IEnumerator Wait(){
		yield return new WaitForSeconds (Timer);
		UnFreezeEnemys();
		ResetOthers();
		
	}

	void UnFreezeEnemys(){
		foreach (GameObject E in Enemys) {
			if(E != null){
				E.GetComponent<MoveEnemy>().enabled = true;
				E.GetComponent<AIManeger>().BackToNeutralVel();
			}
		}
	}
	void ResetOthers(){
		foreach (GameObject OE in OtherEnemys) {
			if(OE != null){
				OE.GetComponent<MoveEnemy>().enabled = false;
				OE.GetComponent<AIManeger>().SlowDown(0);
				OE.GetComponent<AIManeger>().Reset();
			}
		}
	}
}
