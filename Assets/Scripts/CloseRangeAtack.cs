﻿using UnityEngine;
using System.Collections;

public class CloseRangeAtack : MonoBehaviour {

	public float Vel;

	public LayerMask myLayer;

	AIManeger LocalManeger;

	public float CloseRangeDistance;

	Animator Anim;
	void Start () {
		LocalManeger = (AIManeger)GetComponent<AIManeger> ();
		Player = (PlayerTracker)FindObjectOfType ((typeof(PlayerTracker)));
		Anim = GetComponent<Animator> ();
	}
	PlayerTracker Player;
	void Update () {
		Vector3 Target = Player.P_Pos;
		float step = Vel * Time.deltaTime;
		RaycastHit Hit;
		if (Physics.Raycast (transform.position, new Vector3 (0, Target.y - transform.position.y, 0),
		                     out Hit, 0.5f, myLayer)) {
			
			Target = new Vector3 (Target.x, transform.position.y, 0);	
			
		}
		if (Physics.Raycast (transform.position, new Vector3 (Target.x - transform.position.x, 0, 0), 
		                     out Hit, 0.5F, myLayer)) {
			
			Target = new Vector3 (transform.position.x, Target.y, 0);
			
		}
		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (0, Target.y - transform.position.y, 0),
		                     out Hit, 0.5f, myLayer)) {
			
			Target = new Vector3 (Target.x, transform.position.y, 0);	
			
		}
		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (Target.x - transform.position.x, 0, 0), 
		                     out Hit, 0.5f, myLayer)) {
			
			Target = new Vector3 (transform.position.x, Target.y, 0);
			
		}

		if (Player.Distance (transform.position) <= CloseRangeDistance) {
			LocalManeger.CloseEnough = true;
			this.enabled = false;
		}
		transform.position = Vector3.MoveTowards (transform.position, Target, step);


	}
}
