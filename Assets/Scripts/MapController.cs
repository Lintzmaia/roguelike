﻿using UnityEngine;
using System.Collections;

public class MapController : MonoBehaviour {
	public static MapController Map;
	
	float Z;
	void Awake (){
		if (Map == null) {
			DontDestroyOnLoad (this.gameObject);
			Map = this;
		} else if (Map != this) {
			Destroy(gameObject);
		}
		GetComponent<Camera> ().enabled = false;
	}
	void Start () {
		Z = transform.position.z;
		Factor = 2;
	}
	public void Switch(bool C, Vector3 Pos){

		GetComponent<Camera> ().enabled = C;
		transform.position = new Vector3 (Pos.x, Pos.y, Z);

	}
	float VAxis;
	float HAxis;
	float DAxis;

	float Factor;

	void Update () {
		if (GetComponent<Camera> ().enabled) {
			VAxis = Input.GetAxis ("Joystick Vertical") * 5 * Time.deltaTime;
			HAxis = Input.GetAxis ("Joystick Horizontal") * 5 * Time.deltaTime;

			if (Input.GetButtonDown("Zoom")){
				 if( transform.position.z == -40){
					DAxis = 20;
					Factor = 1;
				} else if( transform.position.z == -20){
					Factor = 2;
					DAxis = 10;
				}else if ( transform.position.z == -10){
					Factor = 4;
					DAxis = -30;
				}
			}
			transform.position += new Vector3 (-HAxis, -VAxis, DAxis);
			DAxis = 0;
		}





	}
}
