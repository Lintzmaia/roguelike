﻿using UnityEngine;
using System.Collections;

public class HitPlayer : MonoBehaviour {

	Vector3 PlayerVec;
	float PlayerDist;

	public float Weight;
	public float HitDistance = 0.2f;
	public float CloseRangeSwingDistance = 0.5f;
	public float SwingAngle;
	public Vector3 AtackVec;
	PlayerTracker Player;
	void Start () {
		Player = (PlayerTracker)FindObjectOfType (typeof(PlayerTracker));
	}
	public bool TouchHit;
	public bool SwingHit;

	void Update () {

		if (TouchHit) {
			PlayerDist = Player.Distance (transform.position);
			if (PlayerDist < HitDistance) {

				Player.TakeDamage(Weight);
				TouchHit = false;
			}
		}
	}
	Vector3 DirectionSwing;
	public void PassDir(Vector3 Dir){
		DirectionSwing = Dir;

	}
	public void Swing(){			//Call This function each time The swing happen
		if (Player.Distance (transform.position) <= CloseRangeSwingDistance) {
			PlayerVec = Player.P_Pos - transform.position;
			if (Vector3.Angle (DirectionSwing, PlayerVec) <= SwingAngle) {
				Debug.Log ("HIT");
			}
		}
	}
	public void TouchHitOn(){TouchHit = true;}
	public void TouchHitOff(){TouchHit = false;}
	public void SwingHitOn(){SwingHit = true;}
	public void SwingHitOff(){SwingHit = false;}

}
