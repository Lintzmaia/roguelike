﻿using UnityEngine;
using System.Collections;

public class PlayerTracker : MonoBehaviour {
	public static PlayerTracker Player;
	MapController Map;

	public Vector3 P_Pos;

	public float Health;

	public int Silver;
	public int Copper;
	public int Gold;

	void Awake () {
		if (Player == null) {
			DontDestroyOnLoad (gameObject);
			Player = this;
		} else if (Player != this) {
			Destroy(gameObject);
		}
	}
	void Start(){
		Map = (MapController)FindObjectOfType (typeof(MapController));
	}

	public void TakeDamage(float Weight){
		Debug.Log ("Damage");
	}
	public float Distance(Vector3 ObjPos){
		return (transform.position - ObjPos).magnitude;
	}
	public void AddCoin(string Tag){
		switch (Tag) {
		case "Copper":
			Copper += 1;
			break;
		case "Silver":
			Silver += 1;
			break;
		case "Gold":
			Gold += 1;
			break;
		}
	}
	public float GetHAxis(){
		return GetComponent<MovePlayer> ().GetHAxis ();
	}
	public float GetVAxis(){
		return GetComponent<MovePlayer> ().GetVAxis ();
	}

	public void OpenCloseMap(){
		GetComponent<RotateSprite> ().enabled = !GetComponent<RotateSprite> ().enabled;
		Map.Switch (!GetComponent<RotateSprite> ().enabled, P_Pos);
	}
	public float HAxis;
	public float VAxis;
	void Update () {

		P_Pos = transform.position;
		if (Input.GetButtonDown("Map Key") && GetComponent<MovePlayer>().MovementCheck()){
			OpenCloseMap();
		}

	}
}
