﻿using UnityEngine;
using System.Collections;

public class DealDamege : MonoBehaviour {


	public float DistanceTest = 0.5f;

	public LayerMask InteractbleLayer;

	Vector3 Aim;

	Animator Anim;

	public float WeapeonDamage;
	public float WeapeonForce;
	Transform Player;
	public bool CanDeal;


	public bool DealLeft;
	public float LeftDistance;
	public float LeftUpDistance;
	public float LeftDownDistance;

	public bool DealUp;
	public float UpDistance;
	public float UpLeftDistance;
	public float UpRightDistance;

	public bool DealRight;
	public float RightDistance;
	public float RightUpDistance;
	public float RightDownDistance;

	public bool DealDown;
	public float DownDistance;
	public float DownRightDistance;
	public float DownLeftDistance;


	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
		Anim = GetComponent<Animator> ();
	}

	public void GetAim(Vector3 PassedVec ){
	Aim = PassedVec;
	}

	void Update () {

		if (CanDeal) {
			Vector3 PlayerPos = Player.transform.position;
			RaycastHit Hit;

			if(DealLeft){
				Ray Check1 = new Ray (PlayerPos, Vector3.left);
				Ray Check2 = new Ray (PlayerPos, new Vector3(-0.7f, -0.7f,0));
				Ray Check3 = new Ray (PlayerPos, new Vector3(-0.7f, 0.7f, 0));

				Ray Check4 = new Ray(Vector3.zero, Vector3.zero);
				float distanceCheck4 = 0;



				if (Anim.GetBool("Right to Left")){

					Check4 = new Ray(PlayerPos, Vector3.up);
					distanceCheck4 = UpDistance;

				}else if(Anim.GetBool("Left to Right")){

					Check4 = new Ray(PlayerPos, Vector3.down);
					distanceCheck4 = DownDistance;
				}


				if (Physics.Raycast (Check4, out Hit, distanceCheck4 + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}

				else if (Physics.Raycast (Check3, out Hit, LeftUpDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check2, out Hit, LeftDownDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check1, out Hit, LeftDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}


			}else if(DealUp){

				Ray Check1 = new Ray (Player.position, Vector3.up);
				Ray Check2 = new Ray (Player.position, new Vector3(-0.7f, 0.7f,0));
				Ray Check3 = new Ray (Player.position, new Vector3(0.7f, 0.7f, 0));

				Ray Check4 = new Ray(Vector3.zero, Vector3.zero);
				float distanceCheck4 = 0;

				if (Anim.GetBool("Right to Left")){
					
					Check4 = new Ray(PlayerPos, Vector3.left);
					distanceCheck4 = LeftDistance;
					
				}else if(Anim.GetBool("Left to Right")){
					
					Check4 = new Ray(PlayerPos, Vector3.right);
					distanceCheck4 = RightDistance;
				}
				
				
				if (Physics.Raycast (Check4, out Hit, distanceCheck4 + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				
				else if (Physics.Raycast (Check3, out Hit, UpRightDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check2, out Hit, UpLeftDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check1, out Hit, UpDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}

			}else if(DealDown){

				Ray Check1 = new Ray (Player.position, Vector3.down);
				Ray Check2 = new Ray (Player.position, new Vector3(0.7f, -0.7f,0));
				Ray Check3 = new Ray (Player.position, new Vector3(-0.7f, -0.7f, 0));

				Ray Check4 = new Ray(Vector3.zero, Vector3.zero);
				float distanceCheck4 = 0;

				if (Anim.GetBool("Right to Left")){
					
					Check4 = new Ray(PlayerPos, Vector3.right);
					distanceCheck4 = RightDistance;

				}else if(Anim.GetBool("Left to Right")){
					
					Check4 = new Ray(PlayerPos, Vector3.left);
					distanceCheck4 = LeftDistance;
				}


				if (Physics.Raycast (Check4, out Hit, distanceCheck4 + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				
				else if (Physics.Raycast (Check3, out Hit, DownLeftDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check2, out Hit, DownRightDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check1, out Hit, DownDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
			
			}else if(DealRight){

				Ray Check1 = new Ray (PlayerPos, Vector3.right);
				Ray Check2 = new Ray (PlayerPos, new Vector3(0.7f, 0.7f,0));
				Ray Check3 = new Ray (PlayerPos, new Vector3(0.7f, -0.7f, 0));
				
				Ray Check4 = new Ray(Vector3.zero, Vector3.zero);
				float distanceCheck4 = 0;
				
				if (Anim.GetBool("Right to Left")){
					
					Check4 = new Ray(PlayerPos, Vector3.down);
					distanceCheck4 = DownDistance;
					
				}else if(Anim.GetBool("Left to Right")){
					
					Check4 = new Ray(PlayerPos, Vector3.up);
					distanceCheck4 = UpDistance;
				}
				
				
				if (Physics.Raycast (Check4, out Hit, distanceCheck4 + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				
				 if (Physics.Raycast (Check3, out Hit, RightDownDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						if ((Hit.collider.transform.position - transform.parent.position).magnitude < DistanceTest){
						}
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);

					}
				}
				else if (Physics.Raycast (Check2, out Hit, RightUpDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}
				else if (Physics.Raycast (Check1, out Hit, RightDistance + 0.65f, InteractbleLayer)) {
					if (Hit.collider.gameObject.tag == "Enemy") {
						Hit.collider.GetComponent<KnockBack>().DirectionVector(Aim, WeapeonForce, WeapeonDamage);
					}
				}

			}
		}
	}

}
