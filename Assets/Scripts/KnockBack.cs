﻿using UnityEngine;
using System.Collections;

public class KnockBack : MonoBehaviour {



	Vector3 Direction;

	float KnockBackForce;

	public float Weight = 1;

	public bool Translate;

	Animator Anim;
	void Start(){
		Anim = GetComponent<Animator> ();
	}

	void Update () {
		Ray CheckH = new Ray (transform.position, new Vector3(Direction.x, 0, 0));
		Ray CheckV = new Ray (transform.position, new Vector3 (0, Direction.y, 0));
		RaycastHit Hit;

		if (Physics.Raycast (CheckH, out Hit, 0.5f)) {
			Direction.x = 0;
		}

		if (Physics.Raycast (CheckV, out Hit, 0.5f)) {
			Direction.y = 0;
		}
		if (Translate) {

			transform.position = Vector3.MoveTowards(transform.position, transform.position + Direction.normalized, 
			                                         Time.deltaTime * KnockBackForce);

		} else {
			Direction = new Vector3( 0, 0, 0);
		}
		//if (Anim.GetBool ("Knock Back") != Translate) {
			//Debug.Log("HEY");
			Anim.SetBool ("KnockBack", Translate);
		//}
	}

	public void DirectionVector(Vector3 Dir, float Force, float Damage){
		GetComponent<BoxCollider> ().enabled = false;
		Direction = Dir;
		KnockBackForce = Force / Weight;
		GetComponent<LifeMeter> ().LostHelath (Damage);
		Translate = true;
		Anim.SetBool ("KnockBack", true);

	}
}
