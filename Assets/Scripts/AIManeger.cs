﻿using UnityEngine;
using System.Collections;
using Randon = UnityEngine.Random;

public class AIManeger : MonoBehaviour {

	public bool CanCharge;
	public bool CanCloseAtack;
	public bool CanMoveCloser;

	bool CloseRangeAtack;


	public float DescicionDistance;

	Animator Anim;
	public bool CloseEnough;
	public bool Ready;
	MoveEnemy LocalMoveEnemy;
	CloseRangeAtack LocalCloseRng;
	PlayerTracker Player;

	Vector3 AtackDirection;

	bool Atacking;

	public void Update(){

		if (CanCloseAtack && CanMoveCloser) {

			if (Player.Distance(transform.position) <= DescicionDistance && Ready) {

				if (CloseEnough){
					AtackDirection = Player.P_Pos - transform.position;
					GetComponent<HitPlayer>().PassDir(AtackDirection);
					Ready = false;
					//Pass the preliminary stage on the animator
				}else if( Player.Distance(transform.position)<= DescicionDistance){
					LocalCloseRng.enabled = true;
					LocalMoveEnemy.enabled = false;

				}
				Atacking = true;
				CloseRangeAtack = false;
			} else if(!Atacking){
				
			}
		} else if (CanMoveCloser && CanCharge) {
			if (Player.Distance(transform.position) <= DescicionDistance && Ready) {
				Anim.SetBool ("Charge", true);
			} else {
				GetComponent<MoveEnemy> ().enabled = true;
			}
		}

	}

	Vector3 AwakePos;
	public void SlowDown(int Factor){
		Ready = false;
		GetComponent<MoveEnemy> ().SlowDown (Factor);
		GetComponent<ChargeAtack> ().SlowDown (Factor);
	}
	public void BackToNeutralVel(){
		StopCoroutine (BackToInitialPos ());
		Ready = true;
		GetComponent<MoveEnemy> ().BackToNeutralVel ();
		GetComponent<ChargeAtack> ().BackToNeutralVel ();
	}
	public void Reset(){
		StartCoroutine (BackToInitialPos ());
		GetComponent<MoveEnemy> ().enabled = false;
	}
	IEnumerator BackToInitialPos(){
		yield return new WaitForSeconds(0.5f);
		transform.position = AwakePos;
	}

	public void ResetToIdle(){
		Anim.SetBool ("Charge", false);

	}


	void Start () {
		//Ready = true;
		LocalCloseRng = (CloseRangeAtack)GetComponent<CloseRangeAtack> ();
		LocalMoveEnemy = (MoveEnemy)GetComponent<MoveEnemy> ();
		Player = (PlayerTracker)FindObjectOfType (typeof(PlayerTracker));
		AwakePos = transform.position;
		Anim = GetComponent<Animator> ();
		CloseRangeAtack = false;
	}

}
