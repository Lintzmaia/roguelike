﻿using UnityEngine;
using System.Collections;

public class SwingSword : MonoBehaviour {
	bool[] StateOfAnimation = new bool[2];
	Animator Anim;
	public bool ChangeSpriteLayer;
	bool LookingLeft;
	bool LookingRight;
	bool LookingUp;
	bool LookingDown;

	public float SwingSpeed = 3;
	public bool Swing;

	public bool HoldPlayer;
	public bool RealessePlayer;
	public bool CanCallSwing;
	float timer;
	void Start () {
		CanCallSwing = true;
		Anim = GetComponent<Animator> ();
		timer = 1f;
	}
	void ResetTimer(){
		timer = 0;
	}

	void Update () {
		GetComponent<Animator>().SetBool("Swing", Swing);
		timer += Time.deltaTime;
		StateOfAnimation [0] = Anim.GetBool ("Right to Left");
		StateOfAnimation [1] = Anim.GetBool ("Left to Right");
		if (ChangeSpriteLayer) {
			if (LookingLeft && StateOfAnimation [1]) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player -1";
			} else if (LookingLeft && StateOfAnimation [0]) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player +1";
			} else if (LookingRight && StateOfAnimation [1]) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player +1";
			} else if (LookingRight && StateOfAnimation [0]) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player -1";
			} else if (LookingUp) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player -1";
			} else if (LookingDown) {
				GetComponent<SpriteRenderer> ().sortingLayerName = "Player +1";
			}
		}
		if (CanCallSwing) {
			if (Input.GetButtonDown ("Fire1")) {
				Swing = true;
				GetComponent<Animator>().SetBool("Swing", Swing);
			


				ResetTimer ();
				if (StateOfAnimation [0]) {
					Anim.SetBool ("Right to Left", false);
					Anim.SetBool ("Left to Right", true);
				}
				if (StateOfAnimation [1]) {
					Anim.SetBool ("Left to Right", false);
					Anim.SetBool ("Right to Left", true);
				}
				if (!StateOfAnimation [0] && !StateOfAnimation [1]) {
					Anim.SetBool ("Right to Left", true);
				}
				
			}

		}
		if (HoldPlayer) {
			GetComponentInParent<MovePlayer> ().enabled = false;
		} else if (RealessePlayer) {
			GetComponentInParent<MovePlayer> ().enabled = true;
		}
		Anim.speed = SwingSpeed;
	}

	public void AssagnSwingSpeed(float sliderValue){
		SwingSpeed = sliderValue;
	}
	public void AjustLayerSword(bool up, bool down, bool left, bool right){
						LookingUp = up;
						LookingDown = down;
						LookingLeft = left;
						LookingRight = right;
	}
}
