﻿using UnityEngine;
using System.Collections;

public class ChargeAtack : MonoBehaviour {
	Transform Player;

	public float RaycastDistanceX;
	public float RaycastDistanceY;
	Animator Anim;

	public bool StopCharging;

	public LayerMask myWallLayer;
	public LayerMask myInteractLayer;
	public LayerMask myEntryPoint;

	public bool MakeMove;
	public float Vel = 10;
	Vector3 Target;
	public void PrepareCharge(){
		Target = (Player.position - transform.position).normalized + Player.position;
		MakeMove = true;
	}
	void Start(){
		Anim = GetComponent<Animator> ();
		NeutralVel = Vel;
	}
	public void SlowDown(int Factor){
		if (Factor > 0) {
			Vel = NeutralVel / Factor;
		} else {
			Vel = 0;
		}
	}
	public void BackToNeutralVel(){
		Vel = NeutralVel;
	}
	float NeutralVel;
	void Awake () {
		NeutralVel = Vel;

		Player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
	}

	public bool RaycastOn;
	void Update(){



		RaycastHit Hit;

		if (Physics.Raycast (transform.position, new Vector3 (Target.x - transform.position.x, 0, 0), out Hit, 
		                     RaycastDistanceX, myWallLayer)) {
			Debug.Log ("Raycast");
			Target = new Vector3 (transform.position.x, Target.y, 0);
		}
		if (Physics.Raycast (transform.position, new Vector3 (0, Target.y - transform.position.y, 0), out Hit, 
		                     RaycastDistanceY, myWallLayer)) {

			Target = new Vector3 (Target.x, transform.position.y, 0);
		}

		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (Target.x - transform.position.x, 0, 0), out Hit, 
		                     RaycastDistanceX, myEntryPoint)) {
			Debug.Log ("Raycast");
			Target = new Vector3 (transform.position.x, Target.y, 0);
		}
		if (Physics.Raycast (transform.position + new Vector3(0, 0, 5), new Vector3 (0, Target.y - transform.position.y, 0), out Hit, 
		                     RaycastDistanceY, myEntryPoint)) {
			
			Target = new Vector3 (Target.x, transform.position.y, 0);
		}

		if (RaycastOn) {
			if (Physics.Raycast (transform.position, new Vector3 (Target.x - transform.position.x, 0, 0), out Hit,
			                     RaycastDistanceX, myInteractLayer)) {

				Target = new Vector3 (transform.position.x, Target.y, 0);
			}
			if (Physics.Raycast (transform.position, new Vector3 (0, Target.y - transform.position.y, 0), out Hit,
			                     RaycastDistanceY, myInteractLayer)) {

				Target = new Vector3 (Target.x, transform.position.y, 0);
			}
		}
		if (MakeMove) {
			transform.position = Vector3.MoveTowards(transform.position, Target, Vel * Time.deltaTime);

			if (transform.position == Target || StopCharging){
				GetComponent<AIManeger>().ResetToIdle();
				MakeMove = false;

				//Pass to manager that the processes is done
			}
		}
		if (Input.GetKeyDown (KeyCode.Mouse1)) {
			PrepareCharge();
		}
	}

}
