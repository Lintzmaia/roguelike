﻿using UnityEngine;
using System.Collections;
using Randon = UnityEngine.Random;

public class LifeMeter : MonoBehaviour {
	public GameObject[] Coin;
	public int Amount;

	public float GoldGauge;
	public float SilverGauge;
	public float CopperGauge;

	void Start () {
	
	}
	public bool Calculate;
	public void LostHelath(float Damage){
		if (Calculate) {
			Amount -= Mathf.RoundToInt (Damage);
			Debug.Log (Amount);
			if (Amount <= 0) {
				GetComponent<BoxCollider>().enabled = false;
				GetComponent<AIManeger>().SlowDown(0);
				GetComponent<MoveEnemy> ().enabled = false;
				StartCoroutine (Coroutine ());	
				float Gauge = Randon.Range(0, 10);
				if(Gauge < CopperGauge){
					Instantiate(Coin[0], transform.position, Quaternion.identity);
				}else if( Gauge < SilverGauge){
					Instantiate(Coin[1], transform.position, Quaternion.identity);
				} else{
					Instantiate(Coin[2], transform.position, Quaternion.identity);
				}

				Destroy (gameObject);
			}
		}
	}


	IEnumerator Coroutine(){
		while (GetComponent<SpriteRenderer>().color.a > 0) {
			GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, 0.01f);

			yield return null;
		}
		Destroy (gameObject);

	}
}
