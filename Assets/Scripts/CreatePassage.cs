﻿using UnityEngine;
using System.Collections;

public class CreatePassage : MonoBehaviour {

	public GameObject[] DoorNoCollider;

	void Start(){
		Vector3 Direction = new Vector3 (0, 0, 1);
		switch (name) {
		
		case "Top Door":
			DoorCheck = new Ray(transform.position + new Vector3(0, 1, -3), Direction);
			break;
			
		case "Bottun Door":
			DoorCheck = new Ray(transform.position + new Vector3(0, -1, -3), Direction);
			break;
			
		case "Left Door":
			DoorCheck = new Ray(transform.position + new Vector3(-1, 0, -3), Direction);
			break;
			
		case "Right Door":
			DoorCheck = new Ray(transform.position + new Vector3(1, 0, -3), Direction);
			break;
		default:
			if (transform.rotation.eulerAngles.z == 0){
				DoorCheck = new Ray(transform.position + new Vector3(-1, 0, -3), Direction);
			}else if (transform.rotation.eulerAngles.z == 90){
				DoorCheck = new Ray(transform.position + new Vector3(0, -1, -3), Direction);
			}else if (transform.rotation.eulerAngles.z == 180){
				DoorCheck = new Ray(transform.position + new Vector3(1, 0, -3), Direction);
			}else if (transform.rotation.eulerAngles.z == 270){
				DoorCheck = new Ray(transform.position + new Vector3(0, 1, -3), Direction);
			}
			    break;
		}

		Instantiate(new GameObject("Ray"), DoorCheck.origin, Quaternion.identity);

	}
	
	Ray DoorCheck;
	void Update () {



		RaycastHit Hit;
		if (Physics.Raycast (DoorCheck, out Hit)) {


			if(Hit.collider.tag == "Door"){


				GameObject.Instantiate(DoorNoCollider[0],
				                       Hit.collider.transform.position, 
				                       Quaternion.Euler(new Vector3(0, 0, Hit.transform.rotation.eulerAngles.z)));
				if(Hit.collider.gameObject != null){
					Destroy(Hit.collider.gameObject);
				}
				this.enabled = false;
			} else if (Hit.collider.gameObject.layer == 8){
				GetComponent<BoxCollider>().enabled = false;
				this.enabled = false;
			}else if (Hit.collider.tag == "Border"){
				GameObject.Instantiate(DoorNoCollider[0],
				                       Hit.collider.transform.position, 
				                       Quaternion.Euler(new Vector3(0, 0, Hit.transform.rotation.eulerAngles.z)));
				if(Hit.collider.gameObject != null){
					Destroy(Hit.collider.gameObject);
				}

			}

		}

	}
}
