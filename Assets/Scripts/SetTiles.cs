﻿// Get the Classes (bibliotecs) that will be referred in the code
using UnityEngine; // basic class
using System; // basic class
using System.Collections;
using System.Collections.Generic; // pull the 'List' class, that is, roughlly, a array with dynamic size
using Randon = UnityEngine.Random; // best random generator class in unity

public class SetTiles : MonoBehaviour {

	[Serializable]		//the only reason this is here is because it is prettier in the inspector 
	public class Count	//whem you call the class in the code, take the Obstacle variable in line 25 as exemple
	{
		public int Minimun;
		public int Maximun;

			public Count( int Min, int Max) // With this constructor you can hold 2 values. if this wasan't made, the 
		{									// would became a monster to understand later, because would have to call 
			Minimun = Min;					// two int for a minimun and maximun every time
			Maximun = Max;
		}
	
	}
	// Some nunbers to set the Scene for this screen
	public int lines = 20;									//Number of lines
	public int coluns = 40;									//Nunber of coluns
	public Count Obstacles = new Count (30, 40);			//Holds values that will be used again. A relation beteween area and
															//amount of obstacles can be made during the polishing state

	//Some arrays that form a pool of what tile will be used
	public GameObject[] FloorTiles;
	public GameObject[] Border;
	public GameObject[] Corner;
	public GameObject[] Door;
	public GameObject[] ObstacleTiles;						//Solid object, like walls or trees

	public GameObject Player;


	//Transform is a class that holds Space coordanaes, such as position ( x, y, z ), rotation and scale of the object
	public Transform WorldSpaceBoard;							//Private because no other script will access it
	private List <Vector3> GridPosition = new List<Vector3>();	// a list is just a array that has no defined size

	//Method to make sure there are no data from previous room or scene. Just prepares the variables
	void PreGrid()								
	{
		GridPosition.Clear ();

		for ( int x = 1; x < coluns - 1; x++)	//It starts at one just to make sure the borders will be free, since all points in this list
		{										//have a chance to spawn a obstacle in it.
			for (int y  = 1; y < lines - 1; y++)
			{
				GridPosition.Add(new Vector3(x, y, 0)); //Add the point that any stuff can spawn to the list

			}
		}
	}

	public bool NotFirstBoard;



	public Vector3 PreviousRoom;


	public bool RightDoor;
	public bool BottunDoor;
	public bool LeftDoor;
	public bool TopDoor;

	public LayerMask myLayer;

	public GameObject Room;
	void Start(){		
		NotFirstBoard = false;
		ResetDoorBools ();
	}
	/*public void GetItReady(){
		NotFirstBoard = false;
		ResetDoorBools ();
		Room = GameObject.Find ("Room");
	}*/

	//Spawn a tile
	void GroundAndWalls()
	{		//The reason a game object was created is just to Organize the interface of the engine
		/*if (!NotFirstBoard) {
			PreviousRoom = Room.transform.position;
			Room = new GameObject ("Room");

		}*/
		Room = new GameObject ("Room");
		WorldSpaceBoard = Room.transform;					//Gets the space values of this, 
															//so all calculation are based in a
															//plane that has origin in this object
		Room.transform.position += NewRoomPos;
		for (int x = -1; x < coluns + 1; x++) 
		{
			for(int y = -1; y < lines + 1; y++)
			{

				GameObject floorInPoint = 
					Instantiate( FloorTiles[ Randon.Range (0, FloorTiles.Length)],	//Chooses a tile floor to put in the current point 
					           new Vector3(x, y, 0),								// Position it will spawn
					           Quaternion.identity) as GameObject; 					//quaternion.identity = no rotation
				if (x == -1 || x == coluns || y == -1 || y == lines)		//if the current point it's in the border of the room
				{
					if(Border.Length > 1 && Corner.Length == 0){
					GameObject wallInPoint = Instantiate(Border[Randon.Range(0, Border.Length)],
					                                     new Vector3(x,y,0),
					                                     Quaternion.identity) as GameObject;
					wallInPoint.transform.SetParent(WorldSpaceBoard); 			//Link the wall tile position to the WorldSpaceBoard
					}
					if (Corner.Length == 1 && Border.Length == 1){
						if(x == -1 && y == -1){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0 , 0, 90)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns && y == -1){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 180)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns && y == lines){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 270)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == -1 && y == lines){
							GameObject wallInPoint = Instantiate(Corner[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
							wallInPoint.layer = 11;
							wallInPoint.transform.SetParent(WorldSpaceBoard);

						}
						else if(x == -1){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
							wallInPoint.layer = 9;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(x == coluns){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 180)) as GameObject;
							wallInPoint.layer = 9;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(y == -1){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
							wallInPoint.layer = 10;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
						else if(y == lines){
							GameObject wallInPoint = Instantiate(Border[0], new Vector3(x, y, 0), Quaternion.Euler(0, 0, 270)) as GameObject;
							wallInPoint.layer = 10;
							wallInPoint.transform.SetParent(WorldSpaceBoard);
						}
					}
				}
				floorInPoint.transform.SetParent(WorldSpaceBoard);				//Does the same as above
			}
		}


		ExitLevelTiles (WorldSpaceBoard);
		Room = null;
	}
	public Vector3 NewRoomPos;
	//Generate a the room in the choosen direction
	//The door that the player is crossing, or coming from will be true
	public void SetNewRoomPos(int DoorNunber){
		if (NotFirstBoard) {
			switch (DoorNunber) {

			case 1:
				NewRoomPos = new Vector3 (0, (PreviousRoom.y) + lines + 2, 0);
				break;
			case 2:
				NewRoomPos = new Vector3 (-(PreviousRoom.x) - coluns - 2, 0, 0);
				break;
			case 3:
				NewRoomPos = new Vector3 (0, -(PreviousRoom.y) - lines - 2, 0);
				break;
			case 4:
				NewRoomPos = new Vector3 ((PreviousRoom.x) + coluns + 2, 0, 0);
				break;
			}
		}

	}

	void PreChoosenDoor(){
		
		/*if (RightDoor) {			
			NewRoomPos = new Vector3 (Mathf.Abs (PreviousRoom.x) + coluns + 2, 0, 0);
		}
		if (LeftDoor) {
			NewRoomPos = new Vector3 (-Mathf.Abs (PreviousRoom.x) - coluns - 2, 0, 0);
		}
		if (TopDoor) {
			NewRoomPos = new Vector3 (0, Mathf.Abs (PreviousRoom.y) + lines + 2, 0);
		}
		if (BottunDoor) {
			NewRoomPos = new Vector3 (0, -Mathf.Abs (PreviousRoom.y) - lines - 2, 0);
		}*/

	}
	private int NunOfExits;
	void ExitLevelTiles(Transform WorldSpaceBoard){
		
		if (!NotFirstBoard) {
			NunOfExits = 4;
		} else {
			//PreChoosenDoor ();
			NunOfExits = Randon.Range (0,3 );
		}
		for (int pass = 0; pass < NunOfExits; pass++) {

			Start:		
			switch (Randon.Range (1, 5)) {

				case 1:
				if(!LeftDoor){
					
					float RandomPosition = (int) Randon.Range (1, lines - 1);
					Vector3 DoorPos = new Vector3(- 1, RandomPosition, 3);
					GameObject OrigemRay = new GameObject ("OriginRay");
					OrigemRay.transform.position = DoorPos;
					OrigemRay.transform.SetParent (WorldSpaceBoard);
					Ray CheckOtherRoomHit = new Ray (OrigemRay.transform.position + new Vector3 (-1, 0, 0),
						                        new Vector3 (0, 0, 1));
					RaycastHit Hit;
					if (Physics.Raycast(CheckOtherRoomHit, out Hit)){
						Debug.Log ("AHHHHH Left");
						NunOfExits -= 1;
						LeftDoor = true;
						goto Start;
					}

					Ray CheckHit = new Ray(OrigemRay.transform.position, new Vector3(0, 0, -1));					
					if(Physics.Raycast(CheckHit, out Hit, 10f, myLayer)){
						if (Hit.collider.gameObject.tag != "Door") {
							GameObject.Destroy (Hit.collider.gameObject);
							GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), Quaternion.Euler (0, 0, 0)) as GameObject;
							DoorInPos.gameObject.name = "Left Door";
							DoorInPos.transform.SetParent (WorldSpaceBoard);
						}
						GameObject.Destroy (OrigemRay);
					}
					LeftDoor = true;
					}
					else{goto Start;}
						break;		
				case 2:
				  if(!BottunDoor){
					float RandomPosition = (int) Randon.Range(1, coluns - 1);
					Vector3 DoorPos = new Vector3(RandomPosition,- 1, 3);
					GameObject OrigemRay = new GameObject ("OriginRay");
					OrigemRay.transform.position = DoorPos;
					OrigemRay.transform.SetParent (WorldSpaceBoard);
					Ray CheckOtherRoomHit = new Ray (OrigemRay.transform.position + new Vector3 (0, -1, 0),
						new Vector3 (0, 0, -1));
					RaycastHit Hit;
					if (Physics.Raycast(CheckOtherRoomHit, out Hit)){
						Debug.Log ("AHHHHH BOTTUN");
						NunOfExits -= 1;
						BottunDoor = true;
						goto Start;
					}


					Ray CheckHit = new Ray(OrigemRay.transform.position, new Vector3(0, 0, -1));
					if (Physics.Raycast(CheckHit, out Hit, 10f, myLayer)){
						if (Hit.collider.gameObject.tag != "Door") {
							GameObject.Destroy (Hit.collider.gameObject);
							GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), Quaternion.Euler (0, 0, 90)) as GameObject;
							DoorInPos.gameObject.name = "Bottun Door";
							DoorInPos.transform.SetParent (WorldSpaceBoard);
						}
						GameObject.Destroy (OrigemRay);
					}
					BottunDoor = true;
					}
					else{goto Start;}
					break;

				case 3:
				if(!RightDoor){
					float RandomPosition = (int) Randon.Range(1, lines - 1);
					Vector3 DoorPos = new Vector3( coluns, RandomPosition, 3);
					GameObject OrigemRay = new GameObject ("OriginRay");
					OrigemRay.transform.position = DoorPos;
					OrigemRay.transform.SetParent (WorldSpaceBoard);
					Ray CheckOtherRoomHit = new Ray (OrigemRay.transform.position + new Vector3 (-1, 0, 0),
						new Vector3 (0, 0, -1));
					RaycastHit Hit;
					if (Physics.Raycast(CheckOtherRoomHit, out Hit, 10f, myLayer)){
						Debug.Log ("HIT OTHER ROOM WALL");
						NunOfExits -= 1;

						RightDoor = true;
						goto Start;
					}
					Ray CheckHit = new Ray(OrigemRay.transform.position, new Vector3(0, 0, -1));


					if (Physics.Raycast(CheckHit, out Hit, 10f, myLayer)){
						if (Hit.collider.gameObject.tag != "Door") {
							GameObject.Destroy (Hit.collider.gameObject);
							GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), Quaternion.Euler (0, 0, 180)) as GameObject;
							DoorInPos.gameObject.name = "Right Door";
							DoorInPos.transform.SetParent (WorldSpaceBoard);
						}
						GameObject.Destroy (OrigemRay);

					}
					RightDoor = true;
					}
					else{goto Start;}
					break;

				case 4:
				  if(!TopDoor){
					float RandomPosition = (int) Randon.Range(1, coluns - 1);
					Vector3 DoorPos = new Vector3( RandomPosition, lines, 3);
					GameObject OrigemRay = new GameObject ("OriginRay");
					OrigemRay.transform.position = DoorPos;
					OrigemRay.transform.SetParent (WorldSpaceBoard);
					Ray CheckOtherRoomHit = new Ray (OrigemRay.transform.position + new Vector3 (0, 1, 0),
						new Vector3 (0, 0, -1));
					RaycastHit Hit;
					if (Physics.Raycast(CheckOtherRoomHit, out Hit)){
						NunOfExits -= 1;
						TopDoor = true;
						goto Start;
					}


					Ray CheckHit = new Ray(OrigemRay.transform.position, new Vector3(0, 0, -1));
					if (Physics.Raycast(CheckHit, out Hit, 10f, myLayer)){
						if (Hit.collider.gameObject.tag != "Door") {
							GameObject.Destroy (Hit.collider.gameObject);
							GameObject DoorInPos = Instantiate (Door [Randon.Range (0, Door.Length)], new Vector3 (DoorPos.x, DoorPos.y, 0), Quaternion.Euler (0, 0, 270)) as GameObject;
							DoorInPos.gameObject.name = "Top Door";
							DoorInPos.transform.SetParent (WorldSpaceBoard);
						}
						GameObject.Destroy (OrigemRay);


					}
					TopDoor = true;
				  }
				  else{goto Start;}
						break;					
			}

			if (BottunDoor && TopDoor && RightDoor && LeftDoor) {
				pass = NunOfExits - 1;
			}
			if(pass == NunOfExits - 1){
				ResetDoorBools();
			}
		}
	}

	void ResetDoorBools(){
		RightDoor = false;
		BottunDoor = false;
		LeftDoor = false;
		TopDoor = false;
	}

	// Gets a random point, where something will ramdomlly be put
	Vector3 ObjectPlacementPoint()		//Vector3 because will return a (x, y, z) point in space
	{
		int randomIndexInList = Randon.Range (0, GridPosition.Count);	//ramdomlly gets a vector3 from the list of all availible position
		Vector3 Point = GridPosition [randomIndexInList];			//gets the variable in that index and assing to 'Point'

		GridPosition.RemoveAt (randomIndexInList);			//removes the index, so it don't spawn to trees in the same place

		return Point;	//throw the point back to the function that called it
	}


	//ramdomlly put stuff down in map. Such as loose drops, chests, enemys, walls, rocks
	void LayoutOfLooseObjects(GameObject[] ObjArray,				//Array of the loose object
	                          int minimun, int maximun)
	{	
		Transform SolidHolder = new GameObject("Holder").transform;
		SolidHolder.transform.position = new Vector3 (0, 0, 0);
		int amountOfStuff = 
			Randon.Range (minimun, maximun + 1); 					//A random roll to see the amount of the object that are the array
		for (int pass = 0; pass < amountOfStuff; pass++) 
		{
			Vector3 randomPosition = ObjectPlacementPoint();		//Calls the above method, ObjectPacementPoint, a random amount of times

			GameObject RandomObjectInArray =					//Gets a random object from the poll that is the ObjArray
				ObjArray[Randon.Range(0, ObjArray.Length)];		//Works even if theres only one object in array

			//Spawn the object
			GameObject Solid = Instantiate(RandomObjectInArray,
			            randomPosition,
			            Quaternion.identity) as GameObject;
			Solid.transform.SetParent(SolidHolder);
		}
	}


	public bool GenerateUp;
	public bool GenerateDown;
	public bool GenerateLeft;
	public bool GenerateRight;
	//The method that will call all the others above in the right order
	public void Run()								//Public method because other script will call it
	{


		GroundAndWalls();							//Put down Ground and Walls

		PreGrid ();									//create the list and fill it up with points (x, y, z)

		if (ObstacleTiles.Length > 0) {
			LayoutOfLooseObjects (ObstacleTiles,	//The array of solids
		                      Obstacles.Minimun,	
		                      Obstacles.Maximun);
		}
		//NewRoomPos = new Vector3 (-Mathf.Abs (PreviousRoom.x - coluns - 2), 0, 0);
//		Debug.Log (Room.transform.position + "\n" + PreviousRoom);
		if (GenerateUp) {
			NewRoomPos = new Vector3 (0, (PreviousRoom.y) + lines + 2, 0);
		} else if (GenerateDown) {
			NewRoomPos = new Vector3 (0, -(PreviousRoom.y) - lines - 2, 0);
		} else if (GenerateLeft) {
			NewRoomPos = new Vector3 (-(PreviousRoom.x) - coluns - 2, 0, 0);
		} else if (GenerateRight) {
			NewRoomPos = new Vector3 ((PreviousRoom.x) + coluns + 2, 0, 0);			
		} else {
			NewRoomPos = Vector3.zero;
		}
		/*if (NotFirstBoard) {
			Room.transform.position += NewRoomPos;
		}*/
	}

}
