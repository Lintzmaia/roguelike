﻿using UnityEngine;
using System.Collections;

public class ChangeCameraPos : MonoBehaviour {


	bool MoveCan;
	Vector3 Target;
	float Vel;
	// Use this for initialization
	void Start () {
	
	}
	public void ChangePos(string Dir){
		float Del = 0;
		if (!MoveCan) {
			switch (Dir) {
			case "Up":
				Del = GetComponent<SetTiles> ().lines  + 2;
				Vel = Del * Time.deltaTime;
				Target = transform.position + new Vector3 (0, Del, 0);
				break;
			case "Down":
				Del = GetComponent<SetTiles> ().lines + 2;
				Vel = Del * Time.deltaTime;
				Target = transform.position + new Vector3 (0, -Del, 0);
				break;
			case "Left":
				Del = GetComponent<SetTiles> ().coluns + 2;
				Vel = Del * Time.deltaTime;
				Target = transform.position + new Vector3 (-Del, 0, 0);
				break;
			case "Right":
				Del = GetComponent<SetTiles> ().coluns + 2;
				Vel = Del * Time.deltaTime;
				Target = transform.position + new Vector3 (Del, 0, 0);
				break;
			}
		}
		MoveCan = true;


	}

	void Update(){

		if (MoveCan) {
			transform.position = Vector3.MoveTowards(transform.position, Target, Vel);

			if(transform.position == Target){
				MoveCan = false;
			}
		}
	}


}
