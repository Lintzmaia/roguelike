﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {

	public float Speed = 1;
	public float VerticalRaycastDistance = 0.5f;
	public float HorizontalRaycastDistance = 0.5f;
	public LayerMask myLayerV;
	public LayerMask myLayerH;

	public RaycastHit Hit;
	public Camera Camera;
	void Start(){
		factor = 1;
		Camera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		GetDirection = true;
		CanDogde = true;
	}
	float VAxis;
	float HAxis;
	public float GetHAxis(){return HAxis;}
	public float GetVAxis(){return VAxis;}

	public bool MovementCheck(){
		return GetDirection;
	}

	void Update () {
		if (GetDirection) {
			VAxis = (Input.GetAxis ("Vertical") * Speed * Time.deltaTime);
			HAxis = (Input.GetAxis ("Horizontal") * Speed * Time.deltaTime);
		}

		Ray CheckHorizontalWall;
		Ray CheckVerticalWall;

		CheckHorizontalWall = new Ray (transform.position, new Vector3 (Input.GetAxis("Horizontal"),0 , 0));
		CheckVerticalWall = new Ray(transform.position, new Vector3(0, Input.GetAxis("Vertical"), 0));

		if (Physics.Raycast (CheckHorizontalWall, out Hit, HorizontalRaycastDistance, myLayerH)) {

			if (Hit.collider.tag == "Door") {
				Hit.collider.GetComponent<BoxCollider> ().enabled = false;
			} else {
				HAxis = 0;
			}
		}


		CheckHorizontalWall = new Ray (transform.position, new Vector3 (Input.GetAxis("Horizontal"),0 , 0));
		CheckVerticalWall = new Ray(transform.position, new Vector3(0, Input.GetAxis("Vertical"), 0));
		if (Physics.Raycast (CheckVerticalWall, out Hit, VerticalRaycastDistance, myLayerV)) {


			if (Hit.collider.tag == "Door") {
				Hit.collider.GetComponent<BoxCollider> ().enabled = false;
			} else {
				VAxis = 0;
			}
		}


		GetComponent<ChangeRoom> ().CheckChangeRoom (HAxis, VAxis);
		if (CanDogde) {
			if (Input.GetButtonDown ("Jump")) {
				CanDogde = false;
				GetDirection = false;
				factor = 1.9f;
				StartCoroutine (Timer (0.2f));
				StartCoroutine (Timer2 (0.3f));
			}
		}

		transform.Translate (HAxis * factor , VAxis * factor, 0);
	}
	bool CanDogde;
	float factor;
	bool GetDirection;
	public void StopMove(bool Choice){
		GetDirection = Choice;
	}
	public void ResetFactor(){
		factor = 1;
	}
	IEnumerator Timer2(float Count){
		yield return new WaitForSeconds (Count);
		CanDogde = true;

	}
	IEnumerator Timer(float Count){
		yield return new WaitForSeconds(Count);

		factor = 1;
		GetDirection = true;
	}

}
